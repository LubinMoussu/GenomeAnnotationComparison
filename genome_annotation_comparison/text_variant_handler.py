import logging
import unicodedata
from collections import Counter

import spacy
import re

DEFAULT_SUBDIR_GSPScraper = "text_variant_handler"
logger = logging.getLogger(__name__)


class TextVariantHandler:
    sentence_delimiter_list = [".", ","]
    # roman_number_regex = r"^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$"
    roman_number_regex = r"(IX|IV|V?I{0,3})"
    greek_letter_regex = "([α-ωΑ-Ω]*)"

    def __init__(self):
        self.nlp = spacy.load("en_core_web_sm")

    def is_token_allowed(self, token: spacy.tokens.token.Token) -> bool:
        """
        Only allow valid tokens which are not stop words, spaces and punctuation symbols.
        :param token:
        :return:
        """
        if not isinstance(token, spacy.tokens.token.Token):
            raise TypeError
        if (
                not token
                or not token.string.strip()
                or token.is_stop
                or token.is_punct
                or token.is_space
        ):
            return False
        return True

    def set_custom_boundaries(self, doc: spacy.tokens.doc.Doc):
        """
        customize the sentence detection to detect sentences on custom delimiters.
        :param doc:
        :return:
        """
        # Adds support to use `...` as the delimiter for sentence detection
        for token in doc[:-1]:
            if token.text in self.sentence_delimiter_list:
                doc[token.i + 1].is_sent_start = True
        return doc

    def detect_custom_units(self, text: str):
        """
        divide a text into linguistically meaningful units
        :param text:
        :return:
        """
        self.nlp.add_pipe(self.set_custom_boundaries, before="parser")
        doc = self.nlp(text)
        return list(doc.sents)

    def tokenize_string(self, text: str) -> spacy.tokens.doc.Doc:
        """
        Parse text through the spacy model
        :param annot:
        :return:
        """
        if not isinstance(text, str):
            raise TypeError
        return self.nlp(text)

    def detect_sentences_from_doc(self, doc: spacy.tokens.doc.Doc) -> list:
        """

        :param text:
        :return:
        """
        if not isinstance(doc, spacy.tokens.doc.Doc):
            raise TypeError
        return list(doc.sents)

    def remove_stopwords_and_punctuations_from_sentence(
            self, sentence: spacy.tokens.doc.Doc,
    ):
        """
        Remove StopWords and punctuations
        :param sentence:
        :return:
        """
        return [
            token
            for token in sentence
            if not token.is_stop and not token.is_punct and not token.is_space
        ]

    def lemmatize_token(self, token):
        """
        Reduce token to its lowercase lemma form
        :param token:
        :return:
        """
        return token.lemma_.strip().lower()

    def lemmatize_sentence(self, sentence: spacy.tokens.doc.Doc) -> list:
        """
        Lemmatize the tokens of a doc
        :param sentence:
        :return:
        """
        return [self.lemmatize_token(token) for token in sentence]

    def filter_tokens(self, doc: spacy.tokens.doc.Doc) -> list:
        """
        preprocess complete_doc
        :param doc:
        :return:
        """
        if not isinstance(doc, spacy.tokens.doc.Doc):
            raise TypeError
        complete_filtered_tokens = [
            self.lemmatize_token(token)
            for token in doc
            if self.is_token_allowed(token)
        ]
        return complete_filtered_tokens

    def handle_text_variant(self, annotation: str, filter=False) -> list:
        """

        :param annotation:
        :return:
        """
        if not isinstance(annotation, str) or not isinstance(filter, bool):
            raise TypeError
        sentence = self.tokenize_string(annotation)
        if not filter:
            sentence_cleaned = self.remove_stopwords_and_punctuations_from_sentence(
                sentence=sentence,
            )
            return self.lemmatize_sentence(sentence=sentence_cleaned)
        return self.filter_tokens(sentence)

    def int_to_roman(self, input):
        """ Convert an integer to a Roman numeral. """

        if not isinstance(input, type(1)):
            raise TypeError("expected integer, got %s" % type(input))
        if not 0 < input < 4000:
            raise ValueError("Argument must be between 1 and 3999")
        ints = (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1)
        nums = (
            "M",
            "CM",
            "D",
            "CD",
            "C",
            "XC",
            "L",
            "XL",
            "X",
            "IX",
            "V",
            "IV",
            "I",
        )
        result = []
        for i in range(len(ints)):
            count = int(input / ints[i])
            result.append(nums[i] * count)
            input -= ints[i] * count
        return "".join(result)

    def roman_to_int(self, num: str) -> int:
        """
        Convert Roman number to int
        :param num:
        :return:
        """
        roman_numerals = {
            "I": 1,
            "V": 5,
            "X": 10,
            "L": 50,
            "C": 100,
            "D": 500,
            "M": 1000,
        }
        result = 0
        for i, c in enumerate(num):
            if (i + 1) == len(num) or roman_numerals[c] >= roman_numerals[
                num[i + 1]
            ]:
                result += roman_numerals[c]
            else:
                result -= roman_numerals[c]
        return result

    def find_roman_numbers(self, string: str) -> list:
        """
        Return Roman numbers from a string
        :param string:
        :return:
        """
        # Find all potential matches of a roman number
        match_list = [x.group(1) for x in re.finditer(self.roman_number_regex, string) if x.group()]
        # filter to keep only with space or between brackets
        return [x.group(1) for match in match_list for x in re.finditer(r"[\(\s]({0})".format(match), string) if
                x.group()]

    def replace_roman_numbers(self, string: str) -> str:
        # Find all potential matches of a roman number
        match_list = [x.group(1) for x in re.finditer(self.roman_number_regex, string) if x.group()]
        # filter to keep only with space or between brackets
        for match in match_list:
            for x in re.finditer(r"([\(\s])({0})".format(match), string):
                if x.group():
                    str_nber = str(self.roman_to_int(x.group(2)))
                    string = re.sub(r"{0}{1}".format(x.group(1), match), f"{x.group(1)}{str_nber}", string)
        return string

    def greek_to_name(self, symbol: str) -> str:
        """
        Convert greek symbol to name
        :param symbol:
        :return:
        """
        greek, size, letter, what, *with_tonos = unicodedata.name(symbol).split()
        return what.lower() if size == "SMALL" else what.title()

    def find_greek_letters(self, string: str) -> list:
        """
        Find greek letters in string
        :param string:
        :return:
        """
        match_list = [x.group(1) for x in re.finditer(self.greek_letter_regex, string) if x.group()]
        return match_list

    def replace_greek_letters(self, string: str) -> str:
        """
        Find greek letters in string
        :param string:
        :return:
        """
        match_list = self.find_greek_letters(string)
        for match in match_list:
            if len(match) == 1:
                int_ = self.greek_to_name(match)
                string = re.sub(match, f"{int_}", string)
        return string

    def get_word_frequency(self, words: list):
        """

        :param complete_doc:
        :return:
        """
        word_freq = Counter(words)
        return word_freq

    def preproces_string(self, string: str, filter=False):
        """

        :param string:
        :return:
        """
        string = self.replace_greek_letters(string)
        string = self.replace_roman_numbers(string)
        filtered_tokens = self.handle_text_variant(string, filter)
        return filtered_tokens


