from genome_annotation_comparison.text_variant_handler import (
    TextVariantHandler,
)
from tests.base import BaseTest
import spacy


class TestTextVariantHandler(BaseTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.func1 = "Fumarate hydratase class I, aerobic (EC 4.2.1.2)"

    def setUp(self) -> None:
        self.text_variant_handler = TextVariantHandler()

    def test_TokenizeAnnotation_GivenString_ShouldReturnTextList(self):
        sentence = self.text_variant_handler.tokenize_string(self.func1)
        text_sentence = [elt.text for elt in sentence]
        expected = [
            "Fumarate",
            "hydratase",
            "class",
            "I",
            ",",
            "aerobic",
            "(",
            "EC",
            "4.2.1.2",
            ")",
        ]
        self.assertCountEqual(expected, text_sentence)

    def test_RemoveStopwordsAndPunctuationsFromToken_GivenTokenList_ShouldReturnTextListWithoutStopWordsAndPunctuations(
            self,
    ):
        sentence = self.text_variant_handler.tokenize_string(self.func1)
        sentence_cleaned = self.text_variant_handler.remove_stopwords_and_punctuations_from_sentence(
            sentence,
        )

        text_sentence = [elt.text for elt in sentence_cleaned]
        expected = [
            "Fumarate",
            "hydratase",
            "class",
            "aerobic",
            "EC",
            "4.2.1.2",
        ]
        self.assertCountEqual(expected, text_sentence)

    def test_LemmatizeToken_GivenTokenList_ShouldReturnList(self):
        func1 = "she played chess against rita she likes playing chess."
        sentence = self.text_variant_handler.tokenize_string(func1)
        sentence_lemmatized = self.text_variant_handler.lemmatize_sentence(
            sentence=sentence,
        )

        exepcted = [
            "-pron-",
            "play",
            "chess",
            "against",
            "rita",
            "-pron-",
            "like",
            "play",
            "chess",
            ".",
        ]
        self.assertCountEqual(exepcted, sentence_lemmatized)

    def test_HandleTextVariant_GivenString_ShouldReturnList(self):
        given_then_list = [
            (
                "Fumarate hydratase class I, aerobic",
                ["fumarate", "hydratase", "class", "aerobic"],
            ),
            (
                "Mannose-6-phosphate isomerase",
                ["mannose-6-phosphate", "isomerase"],
            ),
            (
                "Glucuronide transporter UidB",
                ["glucuronide", "transporter", "uidb"],
            ),
            ("Beta-glucuronidase", ["beta", "glucuronidase"]),
            (
                "Maltose regulon regulatory protein MalI (repressor for malXY)",
                [
                    "maltose",
                    "regulon",
                    "regulatory",
                    "protein",
                    "mali",
                    "repressor",
                    "malxy",
                ],
            ),
        ]

        for tuple in given_then_list:
            sentence_lemmatized = self.text_variant_handler.handle_text_variant(
                tuple[0],
            )
            self.assertCountEqual(tuple[1], sentence_lemmatized)

    def test_detectSentences_GivenOneSentenceText_ShouldReturnOneSentence(
            self,
    ):
        test = "anim id est laborum."
        doc = self.text_variant_handler.tokenize_string(test)
        sentences = self.text_variant_handler.detect_sentences_from_doc(doc)
        self.assertEqual(1, len(sentences))

    def test_detectSentences_GivenTwoSentencesText_ShouldReturnTwoSentences(
            self,
    ):
        test = "sunt in culpa qui officia deserunt mollit anim. id est laborum"
        doc = self.text_variant_handler.tokenize_string(test)
        sentences = self.text_variant_handler.detect_sentences_from_doc(doc)
        self.assertEqual(2, len(sentences))

    def test_detectSentences_GivenEmptyText_ShouldReturn(self):
        test = ""
        doc = self.text_variant_handler.tokenize_string(test)
        sentences = self.text_variant_handler.detect_sentences_from_doc(doc)
        self.assertEqual([], sentences)

    def test_detectSentences_GivenInt_ShouldThrowError(self):
        self.assertRaises(
            TypeError, self.text_variant_handler.detect_sentences_from_doc, 4,
        )

    def test_detectCustomUnits_GivenEmptyText_ShouldReturn(self):
        test = ""
        sentences = self.text_variant_handler.detect_custom_units(test)
        self.assertEqual([], sentences)

    def test_detectCustomUnits_GivenTwoSentencesText_ShouldReturnTwoSentences(
            self,
    ):
        test = "sunt in culpa qui officia deserunt mollit anim, id est laborum"
        sentences = self.text_variant_handler.detect_custom_units(test)
        self.assertEqual(2, len(sentences))

    def test_detectCustomUnits_GivenOneSentence_ShouldReturnOneSentence(self):
        self.text_variant_handler.sentence_delimiter_list = [".", ","]
        test = "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt, mollit anim, id est laborum."
        sentences = self.text_variant_handler.detect_custom_units(test)
        self.assertEqual(4, len(sentences))

    def test_filterTokens_GivenSentence_ShouldReturnTokenList(self):
        actual_expect_list = [("Vilitatem occidi orientis ad fixa.", ["vilitatem", "occidi", "orientis", "ad", "fixa"]),
                              ("In his tractibus navigerum nusquam visitur flumen",
                               ['tractibus', 'navigerum', 'nusquam', 'visitur', 'flumen']),
                              ("Hac ita I you, 2! two II 1, i", ['hac', 'ita', '2', 'ii', "1"])
                              ]

        for text, expect in actual_expect_list:
            doc = self.text_variant_handler.tokenize_string(text)
            complete_filtered_tokens = self.text_variant_handler.filter_tokens(doc)
            self.assertCountEqual(
                complete_filtered_tokens,
                expect
            )

    def test_filterTokens_GivenMultipleSentences_ShouldReturnTokenList(self):
        test = "All the world's a stage, And all the men and women merely players. They have their exits and their entrances. And one man in his time plays many parts"
        doc = self.text_variant_handler.tokenize_string(test)
        complete_filtered_tokens = self.text_variant_handler.filter_tokens(doc)
        self.assertCountEqual(
            [
                "world",
                "stage",
                "man",
                "woman",
                "merely",
                "player",
                "exit",
                "entrance",
                "man",
                "time",
                "play",
                "part",
            ],
            complete_filtered_tokens,
        )

    def test_filterTokens_GivenOneWord_ShouldReturnTokenList(self):
        test = "players"
        doc = self.text_variant_handler.tokenize_string(test)
        complete_filtered_tokens = self.text_variant_handler.filter_tokens(doc)
        self.assertCountEqual(["player"], complete_filtered_tokens)

    def test_filterTokens_GivenInt_ShouldRaiseTypeError(self):
        self.assertRaises(
            TypeError, self.text_variant_handler.filter_tokens, "test",
        )

    def test_GreekToName_GivenGreekLetter_ShouldReturnASCIILetter(self):
        given_expected = [("α", "alpha"), ("π", "pi"), ("ψ", "psi")]
        for greek, expected in given_expected:
            letter = self.text_variant_handler.greek_to_name(greek)
            self.assertEqual(letter, expected)

    def test_intToRoman_GivenValidInt_ShouldReturnRoman(self):
        nber_roman_list = [
            (1, "I"),
            (3, "III"),
            (11, "XI"),
            (49, "XLIX"),
            (99, "XCIX"),
            (90, "XC"),
        ]
        for tuple in nber_roman_list:
            roman = self.text_variant_handler.int_to_roman(input=tuple[0])
            self.assertEqual(roman, tuple[1])

    def test_RomanToInt_GivenValidInt_ShouldReturnRoman(self):
        nber_roman_list = [
            (1, "I"),
            (3, "III"),
            (11, "XI"),
            (49, "XLIX"),
            (99, "XCIX"),
            (90, "XC"),
        ]
        for n1, n2 in nber_roman_list:
            roman = self.text_variant_handler.roman_to_int(n2)
            self.assertEqual(roman, n1)

    def test_FindRomanNumbers_GivenRomanNumbers_ShouldReturn(self):

        given_list = ["Shikimate kinase II", "Shikimate kinase IV", "fructose-bisphosphate aldolase class II",
                      "class II ribonucleoside-diphosphate reductase", "DNA polymerase III subunit β",
                      "β-ketoacyl-[acp] synthase III", "β-phenylalanoyl baccatin III-2'-hydroxylase",
                      "oxidocyclase III",
                      "aspartate kinase III", "coproporphyrinogen III oxidase", "oxidase (subunit III)"]

        for elt in given_list:
            result = self.text_variant_handler.find_roman_numbers(elt)
            self.assertTrue(result)

    def test_ReplaceRomanNumbers_GivenRomanNumbers_ShouldReturn(self):

        given_list = [('Shikimate kinase II', 'Shikimate kinase 2'), ('Shikimate kinase IV', 'Shikimate kinase 4'), ('fructose-bisphosphate aldolase class II', 'fructose-bisphosphate aldolase class 2'), ('class II ribonucleoside-diphosphate reductase', 'class 2 ribonucleoside-diphosphate reductase'), ('DNA polymerase III subunit β', 'DNA polymerase 3 subunit β'), ('β-ketoacyl-[acp] synthase III', 'β-ketoacyl-[acp] synthase 3'), ("β-phenylalanoyl baccatin III-2'-hydroxylase", "β-phenylalanoyl baccatin 3-2'-hydroxylase"), ('oxidocyclase III', 'oxidocyclase 3'), ('aspartate kinase III', 'aspartate kinase 3'), ('coproporphyrinogen III oxidase', 'coproporphyrinogen 3 oxidase'), ('oxidase (subunit III)', 'oxidase (subunit 3)')]

        for test, expected in given_list:
            actual = self.text_variant_handler.replace_roman_numbers(test)
            self.assertEqual(actual, expected)

    def test_FindRomanNumbers_GivenNoRomanNumber_ShouldReturnFalse(self):
        given_list = ["T", "A", "P", " P .", "insI3", "dITP/XTP pyrophosphatase", "IS30 transposase", "ISIII30 transposase"]
        for elt in given_list:
            result = self.text_variant_handler.find_roman_numbers(elt)
            self.assertCountEqual(result, [])

    def test_intToRoman_GivenUnvalidInt_ShouldThrowError(self):
        self.assertRaises(
            TypeError, self.text_variant_handler.int_to_roman, "1",
        )

    def test_FindGreekLetters_GivenStringWithGreek_ShouldReturnListWithGreekLetters(self):
        string_list = ["DNA polymerase III subunit β", "transferase subunit α", "α-D-ribose", "decarboxylase αβ subunit", "fructokinase α subunit", "synthase β subunit"]
        for string in string_list:
            match_list = self.text_variant_handler.find_greek_letters(string)
            self.assertTrue(match_list)

    def test_ReplaceGreekLetters_GivenStringWithGreek_ShouldReturnString(self):
        string_list = [('DNA polymerase III subunit β', 'DNA polymerase III subunit beta'), ('transferase subunit α', 'transferase subunit alpha'), ('α-D-ribose', 'alpha-D-ribose'), ('decarboxylase αβ subunit', 'decarboxylase αβ subunit'), ('fructokinase α subunit', 'fructokinase alpha subunit'), ('synthase β subunit', 'synthase beta subunit')]
        for test, expected in string_list:
            actual = self.text_variant_handler.replace_greek_letters(test)
            self.assertEqual(actual, expected)

    def test_HandleTextVariant_GivenValidVariant_ShouldReturnList(self):

        test_list = [('Fumarate hydratase class I, aerobic', ['fumarate', 'hydratase', 'class', '1', 'aerobic']), ('Mannose-6-phosphate isomerase', ['mannose-6-phosphate', 'isomerase']), ('Glucuronide transporter UidB', ['glucuronide', 'transporter', 'uidb']), ('Fructum ordinis quo Ex cum.', ['fructum', 'ordinis', 'quo', 'ex', 'cum']), ('fructokinase α subunit', ['fructokinase', 'alpha', 'subunit']), ('ATP-dependent Clp protease proteolytic subunit 1', ['atp', 'dependent', 'clp', 'protease', 'proteolytic', 'subunit', '1']), ('Acetoacetate metabolism regulatory protein AtoCDNA polymerase III subunit β', ['acetoacetate', 'metabolism', 'regulatory', 'protein', 'atocdna', 'polymerase', '3', 'subunit', 'beta']), ('acetyl-CoA:acetoacetyl-CoA transferase subunit α', ['acetyl', 'coa', 'acetoacetyl', 'coa', 'transferase', 'subunit', 'alpha']), ('Shikimate kinase IV', ['shikimate', 'kinase', '4'])]
        for test, expected in test_list:
            actual = self.text_variant_handler.preproces_string(
                test, True,
            )
            self.assertCountEqual(actual, expected)
