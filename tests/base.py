import logging
import os
from unittest import TestCase

import yaml

import gsp_curation_automation
from gsp_curation_automation.utils.miscellaneous import bytes2file
from gsp_curation_automation.utils.miscellaneous import file2bytes
from gsp_curation_automation.utils.miscellaneous import file2list

logger = logging.getLogger(__name__)


def get_resource_path(filename, dirname=""):
    return os.path.join(
        os.path.dirname(os.path.realpath(__file__)), dirname, filename,
    )


def load_testing_configuration():
    """

    :return:
    """
    logger.info("load testing configuration")

    gsp_curation_automation.TESTING = True
    root_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    file = os.path.join(root_path, "genome_annotation_comparison", "config.yaml")
    with open(get_resource_path(file)) as stream:
        try:
            config = yaml.safe_load(stream)
            cfg = {}
            for k, v in config["TEST"].items():
                if "Path" in k and v:
                    cfg[k] = get_resource_path(v, "data")
                else:
                    cfg[k] = v
            setattr(gsp_curation_automation, "cfg", cfg)
        except yaml.YAMLError as exc:
            print(exc)


class BaseTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # Load configuration
        load_testing_configuration()

        # Empty testing directories

        # Handle logs
        logging.getLogger().setLevel(logging.INFO)

    def check_obj_attrs(self, obj, expected_attrs: dict):
        """
        Test that obj has attrs values
        :param obj:
        :param expected_attrs:
        :return:
        """
        for k, v in expected_attrs.items():
            if isinstance(v, list):
                self.assertCountEqual(v, getattr(obj, k))
            elif isinstance(v, dict):
                self.assertDictEqual(v, getattr(obj, k))
            else:
                self.assertEqual(v, getattr(obj, k))

    @staticmethod
    def load_mocked_response_body(file_in: str):
        """

        :param file_in:
        :return:
        """
        file_in = os.path.join(gsp_curation_automation.mockPath, file_in)
        line_list = file2list(filename=file_in)
        return line_list[0]

    @staticmethod
    def load_mocked_bytes(file_in: str) -> list:
        """

        :param file_in:
        :return:
        """
        file_in = os.path.join(gsp_curation_automation.mockPath, file_in)
        line_list = file2bytes(filename=file_in)
        return line_list
