import logging
import os
import sys
from logging.config import fileConfig

import coloredlogs
import yaml

TESTING = False
LOGGER_CONFIG_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "logging.ini",
)


def get_resource_path(filename, dirname=""):
    return os.path.join(
        os.path.dirname(os.path.realpath(__file__)), dirname, filename,
    )


def get_tests_resource_path(filename, dirname=""):
    return os.path.join(
        os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
        "tests",
        dirname,
        filename,
    )


def init_logger(logger_config_file=LOGGER_CONFIG_FILE):
    """

    :param logger_config_file:
    :return:
    """
    # Define the where to send logs files
    log_path = os.path.join(cfg["root"], "logs")
    sys.path.insert(0, log_path)

    # Read the logging configuration
    logging.config.fileConfig(
        logger_config_file, disable_existing_loggers=True,
    )

    # Set the level for the root logger to DEBUG
    logging.getLogger().setLevel(logging.DEBUG)

    logger = logging.getLogger(__name__)
    logger.propagate = False
    coloredlogs.install(level="INFO", logger=logger)
    # logging.getLogger(__name__).disabled = False

    # HTTP connection
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    # Requests
    logging.getLogger("requests").setLevel(logging.INFO)

    # Decorators log_method
    logging.getLogger("genome_annotation_comparison.utils.decorators").setLevel(
        logging.ERROR,
    )


def load_configuration_from_yaml() -> dict:
    """Load configuration from .yaml file."""
    with open(get_resource_path("config.yaml")) as stream:
        try:
            config = yaml.safe_load(stream)
            cfg = {}
            for k, v in config["DEV"].items():
                if "Path" in k and v:
                    cfg[k] = get_tests_resource_path(v, "data")
                else:
                    cfg[k] = v
            # Save root path
            cfg["root"] = os.path.dirname(
                os.path.dirname(os.path.realpath(__file__)),
            )
            return cfg
        except yaml.YAMLError as exc:
            print(exc)


cfg = load_configuration_from_yaml()
selenium_browser = None
if os.path.isfile(LOGGER_CONFIG_FILE):
    init_logger()
    logging.getLogger(__name__).info("logger initialized")
