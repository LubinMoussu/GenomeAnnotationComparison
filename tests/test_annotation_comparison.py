from genome_annotation_comparison.annotation_comparison import (
    CompareAnnotation,
)
from tests.base import BaseTest


class TestComparePEG(BaseTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.compare_peg = CompareAnnotation()

    def test_isSameEcNumber_2ValidAndEqualEC_ShouldReturnTrue(self):
        ec1, ec2 = "EC 2.6.1.29", "EC 2.6.1.29"
        self.assertTrue(self.compare_peg.is_same_ec_number(ec1, ec2))

    def test_isSameEcNumber_2ValidAndDifferentEC_ShouldReturnTrue(self):
        ec1, ec2 = "EC 2.6.1.29", "EC 2.6.1.7"
        self.assertFalse(self.compare_peg.is_same_ec_number(ec1, ec2))

    def test_isSameEcNumber_2ValidAnd2DifferentEC_ShouldReturnTrue(self):
        ec1, ec2 = "EC 2.6.1.29", "EC 2.6.1.2"
        self.assertFalse(self.compare_peg.is_same_ec_number(ec1, ec2))

    def test_isSameEcNumber_UnvalidEC_ShouldReturnTrue(self):
        ec1, ec2 = 2.6, "EC 2.6.1.7"
        self.assertRaises(
            TypeError, self.compare_peg.is_same_ec_number, ec1, ec2,
        )

    def test_compareEcNumberList_bothListEqual_ShouldReturnTrue(self):
        ec_list1 = ["EC 2.6.1.29"]
        ec_list2 = ["EC 2.6.1.29"]
        self.assertTrue(
            self.compare_peg.is_eclist1_subset_of_eclist2(ec_list1, ec_list2),
        )

    def test_compareEcNumberList_listIsSubSet_ShouldReturnTrue(self):
        ec_list1 = ["EC 2.6.1.29"]
        ec_list2 = ["EC 2.6.1.29", "EC 1.6.1.2"]
        self.assertTrue(
            self.compare_peg.is_eclist1_subset_of_eclist2(ec_list1, ec_list2),
        )

    def test_compareEcNumberList_listIsNotSubSet_ShouldReturnFalse(self):
        ec_list1 = ["EC  1.63.1.57"]
        ec_list2 = ["EC 2.6.1.29"]
        self.assertFalse(
            self.compare_peg.is_eclist1_subset_of_eclist2(ec_list1, ec_list2),
        )

    def test_compareEcNumberList_listIsEmpty_ShouldReturnFalse(self):
        ec_list1 = []
        ec_list2 = ["EC 2.6.1.29"]
        self.assertFalse(
            self.compare_peg.is_eclist1_subset_of_eclist2(ec_list1, ec_list2),
        )

    def test_compareEcNumberList_GiveNotList_ShouldReturnFalse(self):
        ec_list1 = "EC 1.6.1.2"
        ec_list2 = ["EC 2.6.1.29"]
        self.assertRaises(
            TypeError,
            self.compare_peg.is_eclist1_subset_of_eclist2,
            ec_list1,
            ec_list2,
        )

    def test_compareFunctionalRoles_EqualStrings_ShouldReturnTrue(self):
        func1 = "Putrescine aminotransferase"
        func2 = "Putrescine aminotransferase"
        self.assertTrue(
            self.compare_peg.compare_functional_roles_through_jaccard(
                func1, func2,
            ),
        )

    def test_compareFunctionalRoles_SimilarStrings_ShouldReturnTrue(self):
        func1 = "Putrescine aminotransferase"
        func2 = "Putrescine transaminase"
        self.assertTrue(
            self.compare_peg.compare_functional_roles_through_jaccard(
                func1, func2,
            ),
        )

    def test_compareFunctionalRoles_differentStrings_ShouldReturnFalse(self):
        func1 = "Putrescine aminotransferase"
        func2 = "Cadaverine transaminase"
        self.assertFalse(
            self.compare_peg.compare_functional_roles_through_jaccard(
                func1, func2,
            ),
        )

    def test_IsUninformativeAnnotation_GivenTypeInt_ShouldThrowTypeError(
        self,
    ):
        self.assertRaises(
            TypeError, self.compare_peg.is_uninformative_annotation, 2,
        )

    def test_IsUninformativeAnnotation_GivenUninformative_ShouldReturnTrue(
        self,
    ):

        role_list = [
            "hypothetical protein",
            "possible DNA helicase",
            "Hypothetical membrane protein",
            "Possible glycosyltransferase",
            "-like protein",
            "conserved hypothetical protein",
            "Hypothetical protein YggS",
            "FIG039061: hypothetical protein",
            "hypothetical protein related",
            "Hypothetical Coupled",
            "trachomatis hypothetical protein",
        ]
        for role in role_list:
            self.assertTrue(
                self.compare_peg.is_uninformative_annotation(role=role),
            )

    def test_IsUninformativeAnnotation_GivenUninformative_ShouldReturnFalse(
        self,
    ):

        role_list = [
            "NAD(P) transhydrogenase",
            "Protein YdgH",
            "Arginine/ornithine antiporter ArcD",
            "reductase (EC 1.5.1.50)",
            "Inner membrane protein YdgC",
            "Two-component transcriptional response regulator RstA",
            "P450 cytochrome, component",
            "component of G-protein-coupled receptor",
        ]
        for role in role_list:
            self.assertFalse(
                self.compare_peg.is_uninformative_annotation(role=role),
            )

    def test_IsEclist1IntersectingWithEclist2_GivenTwoListsWithCommonElements_ThenReturnTrue(
        self,
    ):
        given_then_list = [
            (["EC 2.6.1.29"], ["EC 2.6.1.29"]),
            (["EC 2.7.1.71"], ["EC 2.6.1.29", "EC 2.7.1.71"]),
            (["EC 2.7.1.71", "EC 2.6.1.57"], ["EC 2.6.1.29", "EC 2.7.1.71"]),
            (
                ["EC 2.7.1.71", "EC 2.6.1.57"],
                ["EC 2.6.1.57", "EC 2.7.1.71", "EC 2.6.1.57"],
            ),
        ]
        for given_then in given_then_list:
            self.assertTrue(
                self.compare_peg.is_eclist1_intersecting_with_eclist2(
                    given_then[0], given_then[1],
                ),
            )

    def test_IsEclist1IntersectingWithEclist2_GivenTwoListsWithNoCommonElements_ThenReturnFalse(
        self,
    ):
        given_then_list = [
            (["EC 2.7.1.71"], ["EC 2.6.1.29"]),
            (["EC 2.7.1.71"], ["EC 2.6.1.29", "EC 2.6.1.57"]),
            (["EC 2.7.1.71", "EC 2.6.1.57"], ["EC 2.6.1.29", "EC 2.6.1.29"]),
            (
                ["EC 2.7.1.71", "EC 2.6.1.57"],
                ["EC 2.6.1.29", "EC 2.7.1.70", "EC 2.6.1.-"],
            ),
        ]
        for given_then in given_then_list:
            self.assertFalse(
                self.compare_peg.is_eclist1_intersecting_with_eclist2(
                    given_then[0], given_then[1],
                ),
            )

    def test_IsEclist1IntersectingWithEclist2_GivenFirstListEmpty_ThenReturnFalse(
        self,
    ):
        given_then_list = [
            ([], ["EC 2.6.1.29"]),
            ([], ["EC 2.6.1.29", "EC 2.6.1.57"]),
            ([], ["EC 2.6.1.29", "EC 2.6.1.29"]),
            ([], ["EC 2.6.1.29", "EC 2.7.1.70", "EC 2.6.1.-"]),
        ]
        for given_then in given_then_list:
            self.assertFalse(
                self.compare_peg.is_eclist1_intersecting_with_eclist2(
                    given_then[0], given_then[1],
                ),
            )

    def test_CompareFunctionalRolesThroughTextVariant_GivenSynonyms_ThenReturnTrue(
        self,
    ):
        synonym_list = [
            ("Shikimate kinase 2", "shikimate kinase 2"),
            ("Shikimate kinase II", "shikimate kinase 2"),
            ("Shikimate kinase IV", "shikimate kinase 4"),
            ("Aspartate aminotransferase", "aspartate aminotransferase"),
            (
                "Arginine--pyruvate transaminase AruH",
                "Arginine--pyruvate transaminase AruH;",
            ),
            (
                "3-deoxy-7-phosphoheptulonate synthase, Phe-sensitive",
                "3-deoxy-7-phosphoheptulonate synthase, Phe-sensitive",
            ),
            (
                "3-dehydroquinate dehydratase I ## AroCI",
                "3-dehydroquinate dehydratase",
            ),
            ("3-dehydroquinate synthase # AroB", "3-dehydroquinate synthase"),
            ("3-dehydroquinate synthase", "3-dehydroquinate synthase"),
            ("2-epi-valiolone synthase", "2-epi-valiolone synthase"),
            (
                "Demethyl 4-deoxygadusol synthase",
                "desmethyl-4-deoxygadusol synthase",
            ),
            (
                "Shikimate/quinate 5-dehydrogenase I beta",
                "quinate/shikimate dehydrogenase",
            ),
            (
                "5-Enolpyruvylshikimate-3-phosphate synthase alpha sub-homology division",
                "3-phosphoshikimate 1-carboxyvinyltransferase",
            ),
            (
                "5-Enolpyruvylshikimate-3-phosphate synthase (alpha sub-homology division",
                "3-phosphoshikimate 1-carboxyvinyltransferase",
            ),
            (
                "UDP-N-acetylglucosamine 1-carboxyvinyltransferase",
                "UDP-N-acetylglucosamine 1-carboxyvinyltransferase",
            ),
        ]
        for tuple in synonym_list:
            func1 = tuple[0].split(" (EC")[0].strip()
            func2 = tuple[1].split(" (EC")[0].strip()
            rbool = self.compare_peg.compare_functional_roles_through_text_variant(
                func1, func2,
            )
            print(func1, func2)
            self.assertTrue(rbool)

        # false_list = [
        #     ("Chorismate mutase I (EC 5.4.99.5) / Prephenate dehydratase (EC 4.2.1.51) # ACT domain", "fused chorismate mutase/prephenate dehydratase (EC 5.4.99.5; EC 4.2.1.51)"),
        #     ("Arogenate dehydratase (EC 4.2.1.91) @ Prephenate dehydratase (EC 4.2.1.51) # ADT1, Transit peptide, ACT domain", "Arogenate dehydratase/prephenate dehydratase 1, chloroplastic; AtADT1; AtPDT1; EC 4.2.1.51; EC 4.2.1.91"),
        #     ("Arogenate dehydratase (EC 4.2.1.91) # ADT3, Transit peptide, ACT domain", "Arogenate dehydratase 3, chloroplastic; AtADT3; Prephenate dehydratase 1; AtPDT1; EC 4.2.1.91"),
        #     ("Prephenate dehydratase (EC 4.2.1.51) # PheAIp/ACT domain", "Prephenate dehydratase; PDT; MjPDT; EC 4.2.1.51"),
        # ]
