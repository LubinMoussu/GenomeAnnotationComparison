import logging
import re
import spacy

from genome_annotation_comparison.text_variant_handler import (
    TextVariantHandler,
)
from genome_annotation_comparison.utils.similaritymeasures import jaccard_similarity

DEFAULT_SUBDIR_GSPScraper = "annotation_comparison"
logger = logging.getLogger(__name__)


class CompareAnnotation:
    EC_NUMBER_PATTERN_REGEX = r"(EC[ :]\d+\.\d+\.[\d-]+\.[\d-]+)"

    uninformative_terms = [
        "hypothetical protein",
        "conserved hypothetical protein",
        "hypothetical",
        "-like protein",
        "uncharacterised",
        "possible",
    ]
    sp = spacy.load("en_core_web_sm")

    def __init__(self, _compare_ec_numbers=True, _jaccard_threshold=0.8):
        """

        """
        self._compare_ec_numbers = _compare_ec_numbers
        self._jaccard_threshold = _jaccard_threshold

    def is_uninformative_annotation(self, role: str) -> bool:
        """
        Given an annotation, evaluate if the annotation is uninformative (i.e. hypothetical protein)

        :param role: annotation
        :return: bool
        """
        if not isinstance(role, str):
            raise TypeError
        role = role.lower()
        for term in self.uninformative_terms:
            if term in role:
                return True
        return False

    def string_to_ec_number(self, s: str) -> list:
        """
        Find EC number(s) in string
        :param s:
        :return:
        """

        if not isinstance(s, str):
            raise TypeError
        try:
            ec_number_list = re.findall(self.EC_NUMBER_PATTERN_REGEX, s)
            ec_number_list = [
                elt.replace("EC:", "EC ") for elt in ec_number_list
            ]
        except IndexError:
            ec_number_list = []
        return list(set(ec_number_list))

    @staticmethod
    def is_same_ec_number(ec1: str, ec2: str) -> bool:
        """

        :param ec1:
        :param ec2:
        :return:
        """
        if not isinstance(ec1, str) or not isinstance(ec2, str):
            raise TypeError
        if ec1 == ec2:
            return True
        return False

    @staticmethod
    def check_eclist_type(ec_list1: list, ec_list2: list) -> bool:
        """

        :param ec_list1:
        :param ec_list2:
        :return:
        """
        if not isinstance(ec_list1, list) or not isinstance(ec_list2, list):
            raise TypeError
        if not ec_list1:
            # Return False to ensure empty is not subset of ec_list2
            return False
        return True

    def is_eclist1_subset_of_eclist2(
            self, ec_list1: list, ec_list2: list,
    ) -> bool:
        """
        Return ec_list1 is subset of ec_list2
        :param ec_list2:
        :param ec_list1:
        :return:
        """
        if self.check_eclist_type(ec_list1, ec_list2):
            return set(ec_list1).issubset(set(ec_list2))

    def is_eclist1_equal_eclist2(self, ec_list1: list, ec_list2: list) -> bool:
        """
        Return ec_list1 is equal to ec_list2
        :param ec_list2:
        :param ec_list1:
        :return:
        """
        if self.check_eclist_type(ec_list1, ec_list2):
            return set(ec_list1) == set(ec_list2)

    def is_eclist1_intersecting_with_eclist2(
            self, ec_list1: list, ec_list2: list,
    ) -> bool:
        if self.check_eclist_type(ec_list1, ec_list2):
            return bool(set(ec_list1).intersection(ec_list2))

    def compare_functional_roles_through_jaccard(
            self, func1: str, func2: str,
    ) -> bool:
        """
        Compare both functional roles with jaccord score
        :param func1:
        :param func2:
        :return:
        """
        if not isinstance(func1, str) or not isinstance(func2, str):
            raise TypeError

        jaccard_score = jaccard_similarity(func1, func2)
        if jaccard_score >= self._jaccard_threshold:
            logger.info(f"{func1} == {func2}")
            return True
        logger.info(f"{func1} != {func2}")
        return False

    def compare_functional_roles_through_text_variant(
            self, func1: str, func2: str,
    ) -> bool:

        if not isinstance(func1, str) or not isinstance(func2, str):
            raise TypeError

        func1_list = TextVariantHandler().handle_text_variant(func1)
        func2_list = TextVariantHandler().handle_text_variant(func2)
        return set(func1_list) == set(func2_list)

    def baseline_comparison(self, func1: str, func2: str) -> bool:

        if not isinstance(func1, str) or not isinstance(func2, str):
            raise TypeError

# TODO process multiple EC numbers attached to the same peg
# if at least one EC number is in the peg, assign the pubmed_id

# TODO process EC numbers with unknown 4th number: EC 4.2.1.-
# TODO proocess superfamily
# TODO process promiscuous enzymes (fig|511145.12.peg.3167, fig|511145.12.peg.1674)
# TODO process mutli-subunits proteins
# TODO process Fumarate hydratase class I, aerobic (EC 4.2.1.2)
# TODO process Thiol-activated cytolysin => tetanolysin O (fig|212717.1.peg.1688)
# TODO process putative, possible: Putative GTP-binding protein YdgA
# TODO Glutarate-semialdehyde dehydrogenase (EC 1.2.1.20); Succinate-semialdehyde dehydrogenase [NAD(P)+] (EC 1.2.1.16)
